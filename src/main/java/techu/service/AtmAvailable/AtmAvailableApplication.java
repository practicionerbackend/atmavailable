package techu.service.AtmAvailable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtmAvailableApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtmAvailableApplication.class, args);
	}

}
