package techu.service.AtmAvailable.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import techu.service.AtmAvailable.mongo.AtmsMongo;
import techu.service.AtmAvailable.mongo.AtmsRepository;

import java.util.*;

@RestController
public class AtmsController {

    @Autowired
    private AtmsRepository repository;

    //Metodo para obtener todos los Cajeros de Colombia
    @GetMapping(value = "/Atms", produces = "application/json")
    @ApiOperation(value="Obtener cajeros", notes="Este método es para obtener todos los Cajeros de Colombia")
    public ResponseEntity<List<AtmsMongo>> getAtms() {
        List<AtmsMongo> list = repository.findAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    //Metodo para obtener todos los Cajeros de una Ciudad
    @GetMapping(value = "/Atms/{ciudad}", produces = "application/json")
    @ApiOperation(value="Obtener cajeros por ciudad", notes="Este método es para obtener todos los Cajeros de una Ciudad")
    public ResponseEntity<List<AtmsMongo>> getAtmsCity
    (@ApiParam(name="ciudad",type="String",value="Ciudad",example="Bogota",required = true)
     @PathVariable String ciudad) {
        List <AtmsMongo> list= repository.findByCity(ciudad);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    //Metodo para obtener todos los Cajeros de una Ciudad por barrio
    @GetMapping(value = "/Atms/{ciudad}/{barrio}", produces = "application/json")
    @ApiOperation(value="Obtener cajeros de una ciudad por barrio", notes="Este método es para obtener todos los Cajeros de un barrio en una ciudad")
    public ResponseEntity<Optional<AtmsMongo>> getAtmsCityLocation
    (@ApiParam(name="ciudad",type="Medellin",value="Ciudad",example = "Medellin",required = true) @PathVariable String ciudad,
     @ApiParam(name="barrio",type="Poblado",value="barrio",example="Poblado",required = true) @PathVariable String barrio){
        Optional <AtmsMongo> list= repository.findByLocation(ciudad, barrio);
        if (list.isPresent()) {
            return new ResponseEntity<>(list, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //Metodo para incluir Cajeros
    @PostMapping(value = "/Atms")
    @ApiOperation(value="Incluir cajeros", notes="Este método es para incluir cajeros")
    public ResponseEntity<String> addAtms
    (@ApiParam(name="nombre cajero",type="String",value="nombre",example="Cajero 0001",required = true)
                @RequestBody AtmsMongo atmsMongo, @RequestHeader("password") String password)  {
        if (password.equals("clavedeseguridad")) {
            AtmsMongo response = repository.insert(atmsMongo);
            return new ResponseEntity<>(response.toString(), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Password inválido",HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping(value = "/Atms/{idcajero}")
    @ApiOperation(value="Modificar datos cajero", notes="Este método modifica los datos de un cajero")
    public ResponseEntity<String> updateatms
    (@ApiParam(name="Id cajero",type="String",value="idcajero",example="0001",required = true)
            @PathVariable String idcajero,
            @RequestBody AtmsMongo atmsMongo,@RequestHeader("password") String password) {
        if (password.equals("clavedeseguridad")) {
            Optional<AtmsMongo> resultado = repository.findByCajero(idcajero);
            if (resultado.isPresent()) {
                AtmsMongo cajeroAModificar = resultado.get();
                cajeroAModificar.nombre = atmsMongo.nombre;
                cajeroAModificar.ciudad = atmsMongo.ciudad;
                cajeroAModificar.barrio = atmsMongo.barrio;
                cajeroAModificar.direccion = atmsMongo.direccion;
                AtmsMongo guardado = repository.save(resultado.get());
                return new ResponseEntity<>(resultado.toString(), HttpStatus.OK);
            } return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        } else{
              return new ResponseEntity<>("Password inválido", HttpStatus.UNAUTHORIZED);
        }
    }

    //Metodo para Eliminar Cajeros por Valor IdCajero
    @DeleteMapping(value = "/Atms/{idcajero}")
    @ApiOperation(value="Eliminar cajero por idcajero", notes="Este método elimina cajeros por idcajero")
    public ResponseEntity<String> deleteAtmsCajeroupdateatms
    (@ApiParam(name="Id cajero",type="String",value="idcajero",example="0001",required = true)
            @PathVariable String idcajero, @RequestHeader("password") String password) {
            if (password.equals("clavedeseguridad")) {
                Optional<AtmsMongo> resultado = repository.findByCajero(idcajero);
                if (resultado.isPresent()) {
                    AtmsMongo cajeroAeliminar = resultado.get();
                    repository.delete(cajeroAeliminar);
                    return new ResponseEntity<>("Se ha eliminado el Cajero", HttpStatus.OK);
                } return new ResponseEntity<String>("Producto no encontrado", HttpStatus.NOT_FOUND);
            }
            else {
                return new ResponseEntity<>("Password inválido", HttpStatus.UNAUTHORIZED);
            }
    }

    //Metodo para Eliminar todos los cajeros
    @DeleteMapping(value = "/Atms")
    @ApiOperation(value="Eliminar todos los cajeros", notes="Este método elimina todos los cajeros")
    public ResponseEntity<String> deleteAtms(@RequestHeader("password") String password) {
        if (password.equals("clavedeseguridad")){
          repository.deleteAll();
          return new ResponseEntity<>("Se han eliminado los Cajeros", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Password inválido", HttpStatus.UNAUTHORIZED);
        }
    }
}
