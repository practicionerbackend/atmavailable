package techu.service.AtmAvailable.mongo;

import io.swagger.annotations.ApiModel;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("ListadosAtms")
@ApiModel
public class AtmsMongo  {

    @Id
    public String id;
    public String idcajero;
    public String nombre;
    public String ciudad;
    public String barrio;
    public String direccion;

    public AtmsMongo() {
    }

    public AtmsMongo(String idcajero, String nombre, String ciudad, String barrio, String direccion) {
        this.idcajero = idcajero;
        this.nombre = nombre;
        this.ciudad = ciudad;
        this.barrio = barrio;
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return String.format("Cajero [Id=%s, IdCajero=%s,Nombre=%s,Ciudad=%s,Barrio=%s,Direccion=%s]",
                id,idcajero,nombre,ciudad,barrio,direccion);
    }

}
