package techu.service.AtmAvailable.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AtmsRepository extends MongoRepository<AtmsMongo, String> {

    @Query("{'ciudad':?0}")
    public List<AtmsMongo> findByCity(String ciudad);

    @Query("{$and:[{'ciudad':?0 },{'barrio':?1 }]}")
    public Optional<AtmsMongo> findByLocation(String ciudad, String barrio);

    @Query("{'idcajero':?0}")
    public Optional<AtmsMongo> findByCajero(String cajero);
}
